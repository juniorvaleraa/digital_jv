# digital_jv

## Name

Product points tracking app

## Description

Show your points history

## SCREENSHOTS

![IMAGE_DESCRIPTION](screenshots/all.png)
![IMAGE_DESCRIPTION](screenshots/toRedeem.png)
![IMAGE_DESCRIPTION](screenshots/redeemed.png)
![IMAGE_DESCRIPTION](screenshots/product.png)

## Installation

yarn install

iOS
npx pod-install

## Usage

yarn ios / yarn android

## Test

yarn test

## License

For open source projects, say how it is licensed.
