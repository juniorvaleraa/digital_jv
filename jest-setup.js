// Import Jest Native matchers
require('react-native-reanimated/lib/module/reanimated2/jestUtils').setUpTests();
require('@shopify/flash-list/jestSetup');

// Silence the warning: Animated: `useNativeDriver` is not supported because the native animated module is missing
jest.mock('react-native/Libraries/Animated/NativeAnimatedHelper');
jest.mock('react-native-reanimated', () =>
  require('react-native-reanimated/mock'),
);
global.fetch = require('jest-fetch-mock');
