import * as React from 'react';
import {render, screen, cleanup} from '../../utils/test-utils';
import ProductView from '../../../src/ui/screens/Product';

const items = [
  {
    createdAt: '2022-12-09T06:34:25.607Z',
    product: 'Handmade Metal Shoes',
    points: 16434,
    image: 'https://loremflickr.com/640/480/transport',
    is_redemption: false,
    id: '1',
  },
  {
    createdAt: '2022-12-09T17:02:51.904Z',
    product: 'Recycled Plastic Tuna',
    points: 92984,
    image: 'https://loremflickr.com/640/480/technics',
    is_redemption: false,
    id: '2',
  },
  {
    createdAt: '2022-12-09T10:20:00.909Z',
    product: 'Fantastic Granite Bacon',
    points: 42416,
    image: 'https://loremflickr.com/640/480/technics',
    is_redemption: false,
    id: '3',
  },
];

describe('Render product view correctly', () => {
  afterEach(cleanup);

  it('shows header with product name', () => {
    const routeMock = {params: {item: {...items[0], name: items[0].product}}};
    render(<ProductView route={routeMock} />);
    const {getByHintText} = screen;

    const headerText = getByHintText('product header');
    expect(headerText).not.toBeEmptyElement();
    expect(headerText).toHaveTextContent(routeMock.params.item.name);
  });

  it('shows product image ', () => {
    const routeMock = {params: {item: {...items[0], name: items[0].product}}};
    render(<ProductView route={routeMock} />);
    const {getByHintText} = screen;

    const productDate = getByHintText('product image');
    expect(productDate).toBeOnTheScreen();
  });

  it('shows product order date ', () => {
    const routeMock = {params: {item: {...items[0], name: items[0].product}}};
    render(<ProductView route={routeMock} />);
    const {getByHintText} = screen;

    const productDate = getByHintText('product order date');
    expect(productDate).not.toBeEmptyElement();
    expect(productDate).toHaveTextContent(routeMock.params.item.createdAt);
  });

  it('shows product points ', () => {
    const routeMock = {params: {item: {...items[0], name: items[0].product}}};
    render(<ProductView route={routeMock} />);
    const {getByHintText} = screen;

    const productPoints = getByHintText('product points');
    expect(productPoints).not.toBeEmptyElement();
    expect(productPoints).toHaveTextContent(routeMock.params.item.points);
  });

  it('navigates back when accept buttom is pressed ', async () => {
    const routeMock = {params: {item: {...items[0], name: items[0].product}}};
    render(<ProductView route={routeMock} />);
    const {getByHintText} = screen;

    const acceptButtom = getByHintText('accept buttom');
    expect(acceptButtom).not.toBeEmptyElement();
    expect(acceptButtom).toBeOnTheScreen();
  });
});
