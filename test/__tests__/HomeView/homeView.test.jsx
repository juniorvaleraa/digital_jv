import * as React from 'react';

import {render, screen, cleanup, fireEvent} from '../../utils/test-utils';
import PointsSection from '../../../src/ui/screens/Home/components/PointsSection/PointsSection';
import ProductCard from '../../../src/ui/screens/Home/components/ProductCard';
import Footer from '../../../src/ui/screens/Home/components/Footer';
import ProductList from '../../../src/ui/screens/Home/components/ProductList';
import productAdapter from '../../../src/data/adapters/productAdapter';
import WelcomeHeader from '../../../src/ui/screens/Home/components/WelcomeHeader';

// Silence the warning https://github.com/facebook/react-native/issues/11094#issuecomment-263240420
// Use with React Native <= 0.63
//jest.mock('react-native/Libraries/Animated/src/NativeAnimatedHelper');

const items = productAdapter([
  {
    createdAt: '2022-12-09T06:34:25.607Z',
    product: 'Handmade Metal Shoes',
    points: 16434,
    image: 'https://loremflickr.com/640/480/transport',
    is_redemption: false,
    id: '1',
  },
  {
    createdAt: '2022-12-09T17:02:51.904Z',
    product: 'Recycled Plastic Tuna',
    points: 92984,
    image: 'https://loremflickr.com/640/480/technics',
    is_redemption: false,
    id: '2',
  },
  {
    createdAt: '2022-12-09T10:20:00.909Z',
    product: 'Fantastic Granite Bacon',
    points: 42416,
    image: 'https://loremflickr.com/640/480/technics',
    is_redemption: false,
    id: '3',
  },
]);

describe('Testing home view', () => {
  beforeEach(() => {
    jest.spyOn(global, 'fetch').mockResolvedValue({
      json: jest.fn().mockResolvedValue(items),
    });
  });

  afterEach(() => {
    jest.restoreAllMocks();
    cleanup();
  });

  it('shows welcome header', async () => {
    const component = <WelcomeHeader />;

    render(component);

    const welcomeBackText = screen.getByHintText('no empty welcome back');
    const userNameText = screen.getByHintText('no empty username');

    expect(welcomeBackText).not.toBeEmptyElement();
    expect(userNameText).not.toBeEmptyElement();

    expect(welcomeBackText).toHaveTextContent('Bienvenido de vuelta!');
    expect(userNameText).toHaveTextContent('Ruben Rodriguez');
  });

  it('shows total points correctly', async () => {
    const expectedPoints = 234232;
    const component = <PointsSection totalPoints={expectedPoints} />;

    render(component);

    const {getByTestId, getByA11yHint} = screen;

    const pointSection = getByTestId('pointsSection');
    const yourPointsText = getByA11yHint('no empty your points');
    const monthPointsText = getByA11yHint('no empty month points');
    const totalPoints = getByA11yHint('no empty total points');

    expect(pointSection).toBeOnTheScreen();
    expect(yourPointsText).not.toBeEmptyElement();
    expect(monthPointsText).not.toBeEmptyElement();
    expect(totalPoints).not.toBeEmptyElement();

    expect(yourPointsText).toHaveTextContent('TUS PUNTOS');
    expect(monthPointsText).toHaveTextContent('Diciembre');
    expect(totalPoints).toHaveTextContent(`${expectedPoints} pts`);
  });

  it('renders productCard correctly', () => {
    const pickedItem = items[0];
    render(
      <ProductCard
        id={pickedItem.id}
        name={pickedItem.product}
        points={pickedItem.points}
        isRedeemed={pickedItem.is_redemption}
        uri={pickedItem.image}
        createdAt={pickedItem.createdAt}
      />,
    );
    const {getByHintText, getByTestId} = screen;
    const productButtom = getByTestId('productCard');
    const productImage = getByHintText('product image');

    expect(productImage).toBeOnTheScreen();
    expect(productButtom).toBeOnTheScreen();
    expect(productButtom).toHaveTextContent(`${pickedItem.product}`);
    expect(productButtom).toHaveTextContent(`${pickedItem.createdAt}`);
    expect(productButtom).toHaveTextContent(`${pickedItem.points}`);
  });

  it('renders footer correctly', () => {
    const onSelectMck = jest.fn();
    render(<Footer onSelect={onSelectMck} />);
    const {getByHintText} = screen;
    const toRedeemBottom = getByHintText('toRedeem buttom');
    const redeemedBottom = getByHintText('redeemed buttom');

    expect(toRedeemBottom).toBeOnTheScreen();
    expect(redeemedBottom).toBeOnTheScreen();
    expect(toRedeemBottom).toHaveTextContent('Ganados');
    expect(redeemedBottom).toHaveTextContent('Canjeados');

    fireEvent.press(toRedeemBottom);

    const allButtom = getByHintText('show all buttom');
    expect(allButtom).toBeOnTheScreen();
    expect(allButtom).toHaveTextContent('Todos');

    fireEvent.press(allButtom);
    expect(getByHintText('toRedeem buttom')).toBeOnTheScreen();
    expect(getByHintText('redeemed buttom')).toBeOnTheScreen();
  });

  it('renders product list correctly', async () => {
    render(<ProductList list={items} />);

    const {getByTestId, getAllByTestId} = screen;
    const list = getByTestId('productList');
    const products = getAllByTestId('productCard');

    expect(products.length).toBe(items.length);
    expect(list).toBeOnTheScreen();
  });
});
