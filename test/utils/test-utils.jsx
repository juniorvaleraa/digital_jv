// test-utils.jsx
import React from 'react';
import {render as rtlRender} from '@testing-library/react-native';
import {NavigationContainer} from '@react-navigation/native';
import {configureStore} from '@reduxjs/toolkit';
import {persistReducer} from 'redux-persist';
import {Provider} from 'react-redux';
import reducers from '../../src/data/store/reducers';
import storage from '../../src/data/store/storage';

import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

const persistConfig = {
  key: 'root',
  version: 1,
  storage,
  whitelist: ['product'],
  timeout: 0,
};

const persistedReducer = persistReducer(persistConfig, reducers);

const render = (
  ui,
  {
    preloadedState,
    store = configureStore({
      reducer: persistedReducer,
      middleware: getDefaultMiddleware =>
        getDefaultMiddleware({
          serializableCheck: false,
        }),
      preloadedState,
    }),
    ...renderOptions
  } = {},
) => {
  const Wrapper = ({children}) => {
    return (
      <Provider store={store}>
        <NavigationContainer>{children}</NavigationContainer>
      </Provider>
    );
  };

  return rtlRender(ui, {wrapper: Wrapper, ...renderOptions});
};

// re-export everything
export * from '@testing-library/react-native';
// override render method
export {render};
