import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'react-redux';

import {PersistGate} from 'redux-persist/integration/react';

import RootNavigation from './src/ui/navigation/RootNavigation';
import {persistor, store} from './src/data/store';

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={undefined} persistor={persistor}>
        <SafeAreaProvider>
          <RootNavigation />
        </SafeAreaProvider>
      </PersistGate>
    </Provider>
  );
}

export default App;
