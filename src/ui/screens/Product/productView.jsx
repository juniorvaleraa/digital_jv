import React from 'react';
import {StyleSheet, View} from 'react-native';
import {RotateInUpLeft, SlideInDown} from 'react-native-reanimated';

import useProductModel from './viewModel';

import AnimatedImage from '../../components/AnimatedImage';
import SubHeaderText from '../../components/SubHeader/SubHeader';
import ProductHeader from './Components/ProductHeader';
import HeaderText from './Components/Header/HeaderText';
import AnimatedButtom from '../../components/AnimatedButtom';
import Card from './Components/Card';

import Colors from '../../theme/Colors';

const ProductView = ({route}) => {
  const {item} = route.params;
  const {onAccept} = useProductModel();

  return (
    <View style={{flex: 1, backgroundColor: Colors.Cultured}}>
      <ProductHeader name={item.name} />
      <View style={styles.layout}>
        <Card>
          <AnimatedImage
            //Tag usado para animación
            entering={RotateInUpLeft.duration(700)}
            accessibilityHint="product image"
            //tag usado para cachear imagen con su id y no con su uri
            source={{uri: item.uri, cacheKey: `ck-${item.id}`}}
            style={styles.image}
          />
        </Card>
        <View style={styles.productDetails}>
          <SubHeaderText>Detalles del producto:</SubHeaderText>
          <HeaderText
            accessibilityHint="product order date"
            textStyle={styles.date}>
            {`Comprado el ${item.createdAt}`}
          </HeaderText>
          <SubHeaderText> Con esta compra acumulaste:</SubHeaderText>
          <HeaderText
            accessibilityHint="product points"
            textStyle={styles.pointsText}>
            {`${item.points} puntos`}
          </HeaderText>
        </View>
        <AnimatedButtom
          accessibilityHint="accept buttom"
          text="Aceptar"
          entering={SlideInDown.delay(150)
            .duration(450)
            .springify()
            .stiffness(40)}
          onPress={onAccept}
          textStyle={styles.acceptText}
        />
      </View>
    </View>
  );
};

export default ProductView;

const styles = StyleSheet.create({
  layout: {flex: 1, padding: 20},
  date: {fontSize: 16},
  pointsText: {fontSize: 24},
  acceptText: {fontSize: 16},
  productDetails: {marginBottom: 40},
  image: {flex: 1},
});
