import {useNavigation} from '@react-navigation/native';

export default function useProductModel() {
  const navigation = useNavigation();

  const onAccept = () => navigation.goBack();

  return {
    onAccept,
  };
}
