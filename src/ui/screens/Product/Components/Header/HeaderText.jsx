import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

import Colors from '../../../../theme/Colors';

const HeaderText = ({textStyle, children, ...props}) => {
  return (
    <View style={styles.container}>
      <Text style={[styles.text, textStyle]} {...props}>
        {children}
      </Text>
    </View>
  );
};

export default HeaderText;

const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
  },
  text: {
    fontFamily: 'Avenir-Black',
    fontSize: 16,
    color: Colors.Black,
  },
});
