import React from 'react';
import {StyleSheet, View} from 'react-native';

import Colors from '../../../../theme/Colors';
import Animated, {RollInRight} from 'react-native-reanimated';

const Card = ({children}) => {
  return (
    <Animated.View
      entering={RollInRight.duration(450)}
      style={styles.container}>
      {children}
    </Animated.View>
  );
};

export default Card;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.White,
    borderRadius: 10,
    padding: 75,
    height: 350,
    marginBottom: 32,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
});
