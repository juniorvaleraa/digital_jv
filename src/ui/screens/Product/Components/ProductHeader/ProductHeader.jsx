import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

import Colors from '../../../../theme/Colors';

const ProductHeader = ({name}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text accessibilityHint="product header" style={styles.headerText}>
          {name}
        </Text>
      </View>
    </View>
  );
};

export default React.memo(ProductHeader);

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.Lavender_Blue,
    height: 150,
    paddingVertical: 24,
    paddingHorizontal: 20,
  },
  header: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'flex-end',
  },
  headerText: {
    color: Colors.Black,
    fontFamily: 'Avenir-Black',
    fontSize: 24,
  },
});
