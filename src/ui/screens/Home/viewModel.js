import {useCallback, useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import {Image} from 'expo-image';
import {
  selectLoadingProducts,
  selectPointsRedeemed,
  selectPointsToRedeem,
  selectProducts,
  selectTotalPoints,
} from '../../../data/store/reducers/features/productSlice';
import useGetProducts from '../../../domain/useCases/useGetProducts';

export default function useHomeViewModel() {
  const redeemedList = useSelector(selectPointsRedeemed);
  const toRedeemList = useSelector(selectPointsToRedeem);
  const products = useSelector(selectProducts);
  const totalPoints = useSelector(selectTotalPoints);
  const isLoading = useSelector(selectLoadingProducts);
  const formattedTotalPoints = Intl.NumberFormat('en-US').format(totalPoints);
  const isEmpty = products.length === 0;
  const [list, setList] = useState([]);
  const {getProducts} = useGetProducts();

  /* 
  como las imagenes van cambiando en cada peticion, hice que se guardaran en cache para poder 
  hacer la animación de shared elements y visualizar la misma imagen la lista asi como en 
  detalles del producto

  Este useEffect se encarga de borrar esa cache cada vez que se reinicia el programa y asi obtener 
  nuevas imagenes
 */
  useEffect(() => {
    async function clearImageCache() {
      await Image.clearDiskCache();
      await Image.clearMemoryCache();
    }

    clearImageCache();
  }, []);

  useEffect(() => {
    getProducts();
  }, []);

  useEffect(() => {
    if (!isLoading && !isEmpty) {
      setList(products);
    }
  }, [isLoading]);

  const onSelectFilter = selectedType => {
    if (selectedType === 'all') setList(products);
    if (selectedType === 'toRedeem') setList(toRedeemList);
    if (selectedType === 'redeemed') setList(redeemedList);
  };

  return {
    list,
    onSelectFilter,
    isLoading,
    isEmpty: products.length === 0,
    totalPoints: formattedTotalPoints,
  };
}
