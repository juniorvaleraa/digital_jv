import React from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
  Pressable,
  Text,
} from 'react-native';
import Colors from '../../theme/Colors';
import {SafeAreaView} from 'react-native-safe-area-context';
import useHomeViewModel from './viewModel';
import ProductList from './components/ProductList';
import SubHeaderText from '../../components/SubHeader';
import Footer from './components/Footer';
import WelcomeHeader from './components/WelcomeHeader';
import PointsSection from './components/PointsSection';
import LoadingIndicator from '../../components/LoadingIndicator';

export default function HomeView() {
  const {totalPoints, isLoading, isEmpty, list, onSelectFilter} =
    useHomeViewModel();

  if (isLoading && isEmpty) {
    return (
      <View style={styles.emptyContainer}>
        <LoadingIndicator />
      </View>
    );
  }

  if (!isLoading && isEmpty) {
    return (
      <View style={styles.emptyContainer}>
        <SubHeaderText>Houston, tenemos un problema</SubHeaderText>
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <WelcomeHeader />
      <PointsSection totalPoints={totalPoints} />
      <SubHeaderText>TUS MOVIMIENTOS</SubHeaderText>
      <View style={styles.listContainer}>
        <ProductList list={list} />
      </View>
      <Footer onSelect={onSelectFilter} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  emptyContainer: {flex: 1, alignItems: 'center', justifyContent: 'center'},
  container: {
    flex: 1,
    backgroundColor: Colors.Cultured,
    paddingHorizontal: 20,
  },
  listContainer: {
    padding: 5,
    borderRadius: 10,
    height: 350,
    paddingVertical: 23,
    paddingHorizontal: 10,
    backgroundColor: Colors.White,
    marginBottom: 43,
  },
});
