import React from 'react';
import {StyleSheet, View} from 'react-native';

const YourPointsWrapper = ({children}) => {
  return (
    <View testID="pointsSection" style={styles.container}>
      {children}
    </View>
  );
};

export default YourPointsWrapper;

const styles = StyleSheet.create({
  container: {marginBottom: 20},
});
