import React from 'react';
import {Image} from 'expo-image';
import {Pressable, StyleSheet, Text, View} from 'react-native';
import Colors from '../../../../theme/Colors';
import {useNavigation} from '@react-navigation/native';
import AnimatedImage from '../../../../components/AnimatedImage';
import Icon from '../../../../assets/images';

const ProductCard = ({name, uri, id, isRedeemed, createdAt, points}) => {
  const colorTextStyle = isRedeemed
    ? {color: Colors.Red}
    : {color: Colors.Dark_Pastel_Green};

  const redeemStatus = isRedeemed ? '-' : '+';
  const navigation = useNavigation();

  const onProductPress = () => {
    navigation.navigate('ProductDetails', {
      item: {name, uri, id, createdAt, points},
    });
  };

  return (
    <Pressable
      testID="productCard"
      onPress={onProductPress}
      style={styles.container}>
      <AnimatedImage
        accessibilityHint="product image"
        //tag es para cuando se esta usando una implementación basada em recycle view como Flashlist
        recyclingKey={`image-${id}`}
        style={styles.image}
        source={{uri, cacheKey: `ck-${id}`}}
      />
      <View style={styles.productDetails}>
        <Text numberOfLines={1} ellipsizeMode="tail" style={styles.nameText}>
          {name}
        </Text>
        <Text style={styles.createdAtText}>{createdAt}</Text>
      </View>
      <View style={styles.points}>
        <View style={styles.redeemPoints}>
          <Text style={[styles.redeemStatusText, colorTextStyle]}>
            {redeemStatus}
          </Text>
          <Text style={styles.pointsText}>{`${points}`}</Text>
        </View>
        <Image
          style={styles.iconImage}
          contentFit="cover"
          source={Icon.Arrow}
        />
      </View>
    </Pressable>
  );
};

export default ProductCard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 8,
  },
  redeemStatusText: {
    fontSize: 16,
    fontWeight: '800',
  },
  iconImage: {width: 10, height: 10},
  image: {
    width: 55,
    height: 55,
    borderRadius: 10,
  },
  productDetails: {
    flex: 1,
    marginLeft: 11,
    paddingTop: 5,
    paddingBottom: 8,
    justifyContent: 'space-between',
  },
  nameText: {
    fontSize: 14,
    fontFamily: 'Avenir-Black',
    color: Colors.Black,
  },
  createdAtText: {
    fontSize: 12,
    fontFamily: 'Avenir-Book',
    fontWeight: '400',
  },
  points: {
    flex: 0.7,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  redeemPoints: {
    flexDirection: 'row',
    marginRight: 23,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pointsText: {
    fontSize: 16,
    fontWeight: '800',
    color: Colors.Black,
    fontFamily: 'Avenir-Black',
  },
});
