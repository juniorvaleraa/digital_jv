import {StyleSheet, View} from 'react-native';
import React, {useState} from 'react';
import AnimatedButtom from '../../../../components/AnimatedButtom';
import {SlideInDown, SlideInLeft} from 'react-native-reanimated';

const Footer = ({onSelect}) => {
  const [showAll, setShowAll] = useState(false);

  const onPressShow = selectedType => {
    setShowAll(prev => !prev);
    onSelect(selectedType);
  };

  if (showAll) {
    return (
      <View style={styles.allButtom}>
        <AnimatedButtom
          accessibilityHint="show all buttom"
          text="Todos"
          textStyle={styles.allButtomText}
          entering={SlideInDown.duration(250)}
          onPress={() => onPressShow('all')}
        />
      </View>
    );
  }
  return (
    <View style={styles.redeemButtoms}>
      <View style={styles.toRedeemButtom}>
        <AnimatedButtom
          accessibilityHint="toRedeem buttom"
          text="Ganados"
          entering={SlideInLeft.duration(250)}
          onPress={() => onPressShow('toRedeem')}
        />
      </View>
      <View style={styles.redeemedButtom}>
        <AnimatedButtom
          accessibilityHint="redeemed buttom"
          text="Canjeados"
          entering={SlideInLeft.duration(250)}
          onPress={() => onPressShow('redeemed')}
        />
      </View>
    </View>
  );
};

export default Footer;

const styles = StyleSheet.create({
  toRedeemButtom: {flex: 1, marginRight: 13},
  redeemedButtom: {flex: 1},
  redeemButtoms: {width: '100%', flexDirection: 'row'},
  allButtom: {width: '100%'},
  allButtomText: {fontSize: 16},
});
