import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Colors from '../../../../theme/Colors';

function WelcomeHeader() {
  return (
    <View style={styles.container}>
      <Text
        accessibilityHint="no empty welcome back"
        style={styles.welcomeText}>
        Bienvenido de vuelta!
      </Text>
      <Text accessibilityHint="no empty username" style={styles.userText}>
        Ruben Rodriguez
      </Text>
    </View>
  );
}

export default React.memo(WelcomeHeader);

const styles = StyleSheet.create({
  container: {marginBottom: 20},
  welcomeText: {
    color: Colors.Rich_Black,
    fontSize: 20,
    fontFamily: 'Avenir-Black',
    fontWeight: '800',
  },
  userText: {fontSize: 16, fontWeight: '400', fontFamily: 'Avenir-Black'},
});
