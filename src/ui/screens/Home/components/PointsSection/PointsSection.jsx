import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import YourPointsWrapper from '../YourPointsWrapper/YourPointsWrapper';
import SubHeaderText from '../../../../components/SubHeader';

import Colors from '../../../../theme/Colors';

function PointsSection({totalPoints}) {
  return (
    <YourPointsWrapper>
      <SubHeaderText accessibilityHint="no empty your points">
        TUS PUNTOS
      </SubHeaderText>
      <View style={styles.yourPointsCard}>
        <View style={styles.yourPointsDate}>
          <Text
            accessibilityHint="no empty month points"
            style={styles.yourPointsDateText}>
            Diciembre
          </Text>
        </View>
        <View style={styles.yourTotalPoints}>
          <Text
            accessibilityHint="no empty total points"
            style={styles.yourTotalPointsText}>{`${totalPoints} pts`}</Text>
        </View>
      </View>
    </YourPointsWrapper>
  );
}
export default React.memo(PointsSection);

const styles = StyleSheet.create({
  container: {marginBottom: 20},

  yourPointsCard: {
    alignSelf: 'center',
    backgroundColor: Colors.Ultramarine_Blue,
    borderRadius: 20,
    height: 143,
    width: 286,
  },
  yourPointsDate: {
    width: '100%',
    paddingLeft: 18,
    paddingTop: 21,
    marginBottom: 7,
  },
  yourPointsDateText: {
    fontSize: 16,
    color: Colors.White,
    fontFamily: 'Avenir-Black',
  },
  yourTotalPoints: {
    width: '100%',
    alignItems: 'center',
    flex: 1,
  },
  yourTotalPointsText: {
    fontSize: 32,
    color: Colors.White,
    fontFamily: 'Avenir-Black',
  },
});
