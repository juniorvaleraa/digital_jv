import React from 'react';
import ProductCard from '../ProductCard';
import {FlashList} from '@shopify/flash-list';

const keyExtractor = item => `${item.id}`;

const renderItem = ({item}) => {
  return (
    <ProductCard
      name={item.product}
      id={item.id}
      isRedeemed={item.is_redemption}
      uri={item.image}
      points={item.points}
      createdAt={item.createdAt}
    />
  );
};

const ProductList = ({list}) => {
  return (
    <FlashList
      testID="productList"
      data={list}
      keyExtractor={keyExtractor}
      showsVerticalScrollIndicator={false}
      bounces={false}
      renderItem={renderItem}
      estimatedItemSize={55}
    />
  );
};

export default ProductList;
