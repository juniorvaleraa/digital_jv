import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Colors from '../../theme/Colors';

const SubHeaderText = ({children, ...props}) => {
  return (
    <View style={styles.yourPointsHeader}>
      <Text style={styles.yourPointsText} {...props}>
        {children}
      </Text>
    </View>
  );
};

export default SubHeaderText;

const styles = StyleSheet.create({
  yourPointsHeader: {marginBottom: 20},
  yourPointsText: {
    fontSize: 14,
    color: Colors.Spanish_Gray,
    fontFamily: 'Avenir-Black',
  },
});
