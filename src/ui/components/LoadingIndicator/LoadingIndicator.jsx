import React from 'react';
import {ActivityIndicator} from 'react-native';

import Colors from '../../theme/Colors';

const LoadingIndicator = () => {
  return <ActivityIndicator size="large" color={Colors.Vampire_Black} />;
};

export default LoadingIndicator;
