import React from 'react';
import Animated from 'react-native-reanimated';
import {Pressable, StyleSheet, Text} from 'react-native';
import Colors from '../../theme/Colors';

const AnimatedPressable = Animated.createAnimatedComponent(Pressable);

export default function AnimatedButtom({
  onPress,
  text,
  textStyle,
  children,
  ...props
}) {
  return (
    <AnimatedPressable onPress={onPress} style={styles.container} {...props}>
      <Text style={[styles.text, textStyle]}>{text}</Text>
      {children}
    </AnimatedPressable>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.Ultramarine_Blue,
    borderRadius: 10,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 12,
    color: Colors.White,
    fontFamily: 'Avenir-Black',
  },
});
