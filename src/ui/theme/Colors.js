const Colors = {
  Vampire_Black: '#070707',
  Ultramarine_Blue: '#334FFA',
  Dark_Pastel_Green: '#00b833',
  Red: '#FF0000',
  Rich_Black: '#020202',
  Spanish_Gray: '#989B9B',
  Lavender_Blue: '#cfd6ff',
  Black: '#000000',
  White: '#FFFFFF',
  Cultured: '#F8F8F8',
};

export default Colors;
