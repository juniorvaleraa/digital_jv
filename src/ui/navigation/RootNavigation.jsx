import React from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeView from '../screens/Home';
import ProductView from '../screens/Product';

const {Navigator, Screen} = createNativeStackNavigator();

export const AppNavigator = () => {
  return (
    <Navigator initialRouteName="Home" screenOptions={{headerShown: false}}>
      <Screen name="Home" component={HomeView} />
      <Screen name="ProductDetails" component={ProductView} />
    </Navigator>
  );
};

export default function RootNavigation() {
  return (
    <NavigationContainer>
      <AppNavigator />
    </NavigationContainer>
  );
}
