import {createAsyncThunk, createSelector, createSlice} from '@reduxjs/toolkit';
import api from '../../../services/api';
import productAdapter from '../../../adapters/productAdapter';

const initialState = {
  products: [],
  loading: false,
  error: undefined,
};

export const getProductsList = createAsyncThunk(
  'product/getProductsList',
  async (__, {rejectWithValue}) => {
    try {
      const URL = 'https://6222994f666291106a29f999.mockapi.io/api/v1/products';
      const response = await api.get(URL);

      return productAdapter(response);
    } catch (error) {
      return rejectWithValue(error.message);
    }
  },
);

const onSuccess = (state, action) => {
  state.products = action.payload;
  state.loading = false;
};

const onPending = state => {
  state.loading = true;
};
const onError = (state, action) => {
  state.error = action.payload;
  state.loading = false;
};

const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    setProducts: (state, action) => {
      state.products = action.payload;
    },
  },
  extraReducers: builder => {
    builder
      .addCase(getProductsList.pending, onPending)
      .addCase(getProductsList.rejected, onError)
      .addCase(getProductsList.fulfilled, onSuccess);
  },
});

export const {setProducts} = productSlice.actions;

export default productSlice.reducer;

export const selectProducts = state => state.product.products;
export const selectLoadingProducts = state => state.product.loading;

//Seleciona los productos que no han sido canjeados
export const selectPointsToRedeem = createSelector(
  [selectProducts],
  products => {
    return products.filter(product => product.is_redemption === false);
  },
);

//Seleciona los productos que han sido canjeados
export const selectPointsRedeemed = createSelector([selectProducts], products =>
  products.filter(product => product.is_redemption === true),
);

export const selectTotalPoints = createSelector(
  [selectPointsToRedeem],
  products => products.reduce((a, b) => a + b.points, 0),
);
