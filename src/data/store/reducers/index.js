import {combineReducers} from 'redux';
import productSlice from './features/productSlice';

const reducers = combineReducers({
  product: productSlice,
});

export default reducers;
