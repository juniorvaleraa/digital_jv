const options = {
  month: 'long',
  day: 'numeric',
};

const productAdapter = products => {
  return products.map(item => {
    const date = new Date(item.createdAt);
    const dayAndMonth = date.toLocaleDateString('es-MX', options);
    return {
      ...item,
      createdAt: `${dayAndMonth}, ${date.getFullYear()}`,
    };
  });
};

export default productAdapter;
