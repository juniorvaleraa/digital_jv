import {useDispatch} from 'react-redux';
import {getProductsList} from '../../data/store/reducers/features/productSlice';

export default function useGetProducts() {
  const Dispatch = useDispatch();

  return {
    getProducts: () => Dispatch(getProductsList()),
  };
}
